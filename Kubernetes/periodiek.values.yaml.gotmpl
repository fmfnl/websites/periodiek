app:
  image:
    repository: registry.gitlab.com/fmfnl/websites/periodiek
    tag: {{ requiredEnv "TAG" }}

  periosMount:
    claimName: periodiek-drive
    path: Published

  nodeSelector:
    kubernetes.io/os: "linux"
  tolerations:
    - key: "cattle.io/os"
      value: "linux"
      operator: "Equal"
      effect: "NoSchedule"
    - key: "cattle.io/os"
      value: "linux"
      operator: "Equal"
      effect: "PreferNoSchedule"

ingress:
  tls:
    enabled: true
    clusterIssuer: {{ .Values.clusterIssuers.http }}
  
  dns:
    enabled: true
    cname: rancher.{{ .Values.environment }}.fmf.nl

  hostnames:
    - host: perio.{{ .Values.domain }}

{{- if env "CI_DEPLOY_USER" }}
imagePullSecret:
  username: {{ env "CI_DEPLOY_USER" }}
  password: {{ env "CI_DEPLOY_PASSWORD" }}
  registry: {{ env "CI_REGISTRY" }}
{{- end }}

buildID: {{ env "BUILDID" | default (requiredEnv "TAG") }}

{{- if env "CI_ENVIRONMENT_SLUG" }}
gitlab:
  app: {{ env "CI_PROJECT_PATH_SLUG" }}
  env: {{ env "CI_ENVIRONMENT_SLUG" }}
{{- end }}
