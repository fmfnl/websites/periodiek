# Periodiek
    Copyright (C) 2023  Fysisch-Mathematische Faculteitsvereniging
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.
    
    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

This directory contains the app for the Periodiek website. The implementation makes use of the [MVC (Model-View-Controller) pattern](https://learn.microsoft.com/en-us/aspnet/core/mvc/overview). 
The models, views and controllers are provided in the respective directories. It also uses the [Repository pattern](https://learn.microsoft.com/en-us/aspnet/mvc/overview/older-versions/getting-started-with-ef-5-using-mvc-4/implementing-the-repository-and-unit-of-work-patterns-in-an-asp-net-mvc-application)
and the [Options pattern](https://learn.microsoft.com/en-us/aspnet/core/fundamentals/configuration/options). Those can be found in the respective 
directories as well.

For working with this code, I recommend to use Visual Studio Code (Win/Mac/Linux, with the C# plugin), the full Visual Studio IDE (Win/Mac) or JetBrains Rider (Win/Mac/Linux). Within VS Code, you can simply open the root directory of this repository; for full VS and JetBrains Rider I recommend to open just the `Periodiek.csproj` file - it will load all other things required.

## Getting Started

The best way to run this application is using Docker (Compose), but for development a local runtime might be more convenient. Instructions for both of them follow.

The first step is to copy the `.env.example` file to `.env`, and make changes to the config as you desire. Make sure to copy the folder with the perios from Perio over to the directory you specify in this `.env` directory.

### Local Runtime

Prerequisites:

- The .NET 7.0 SDK and ASP.NET Core 7.0 Runtime ([instructions](https://dotnet.microsoft.com/en-us/download)) - you should be able to run `dotnet --list-sdks` and have at least some version `7.x.x` installed.

You should now be able to run `dotnet watch run` in the current directory, and it will start the webserver, open your browser, and take you to the homepage.

### Docker setup

This application can be run using Docker. You will need to install Docker on your own PC. Instructions can be found all over the internet, [for Windows](https://docs.docker.com/desktop/windows/install/), [for Mac](https://docs.docker.com/desktop/mac/install/), as well as [for Ubuntu](https://docs.docker.com/engine/install/ubuntu/) and [other distros](https://docs.docker.com/engine/install/). On Linux distributions, Docker Compose should be installed [separately](https://docs.docker.com/compose/install/linux/).

Once you are set up, ensure you are running Docker Engine version >=19.03 by running `docker --version`, and you should make sure Docker Compose is installed by running `docker compose --help`.

Finally, you can get going by running `docker compose up` in the project main/root directory. It should now automatically build the docker image and start the web server.

## What's in the box

This application is a fairly standard .NET Core MVC implementation without a database connection as we will not need that for now. Very few third-party packages are included.

## How configuration works

There are multiple ways to configure the website with all the information required (such as database credentials) to get the app running.

- **Through environment variables**: This is how Docker configures the app when running inside Docker. The `docker-compose.yml` file in the repository root can set properties as environment variables.
- **Through `launchSettings.json`**: This file contains some settings that will be applied, similar to the environment variables in Docker.
- **Through `appsettings.x.json` files**: This method is the primary way of setting configuration.

In practice, during startup, .NET will take various configuration sources and merge them together, in the following order:

1. `appsettings.json`
2. `appsettings.<Environment>.json`
3. Environment variables
4. Command-line arguments

## MAKE SURE TO NEVER COMMIT ANY PASSWORDS IN THE GIT REPOSITORY
