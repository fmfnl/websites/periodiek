// There is a lot of syntactic sugar in this file,
// or well, lack of code. Modern C# has many shorthands that allow
// us to prevent writing boilerplate for Main() and such. So basically,
// we are dropped in Main() immediately here.

using System.Text.Json.Serialization;

// ---------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------

// Setting up a .NET Core application (Web App, Console App, whatever) typically has
// two "phases": we first configure the IoC/DI system (Inversion of Control, Dependency
// Injection), which will allow us to easily switch implementations/settings without
// changing the real application code. This includes registering any policies/connections/
// whatever we might be using - even if we don't always end up using them.
// In the second step we define how the app should behave when a request comes in,
// and what it should do when. That part is typically shorter.

// We can now properly start setting up and configuring our application class.
// We start by creating a WebApp "Builder" for configuration
var builder = WebApplication.CreateBuilder(args);

// We then register all services to the IoC container, this will
// allow us to inject them into our controllers and other classes.

// Say we will be using controller classes
builder.Services.AddControllersWithViews();

// Register a CORS policy that allows traffic from all sources (do NOT use this
// in this form for real production applications!)
builder.Services.AddCors(options =>
{
    options.AddPolicy("default", builder =>
        builder
            .AllowAnyHeader()
            .AllowAnyMethod()
            .AllowAnyOrigin()
    );
});

// ---------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------

// Then we build the app and its IoC container, so we can configure
// the lifecycle, including routing and middleware.
var app = builder.Build();

// Specify which CORS policy to use - registered above
app.UseCors("default");

// If we are not in development, we enable the error pages
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
}

app.UseStaticFiles();
app.UseRouting();

// Find all Controller classes/implementations in the current application and
// register their routes and operations.
app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}"
);

// Run the app
app.Run();
