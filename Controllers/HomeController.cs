using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using FMF.Periodiek.Models;
using Microsoft.AspNetCore.Mvc;

namespace FMF.Periodiek.Controllers;

[Controller] // denotes this class is a controller
public class HomeController : Controller
{
    public async Task<IActionResult> Index()
    {
        var exams = new List<Perio> {
            new Perio { Name = "2022 Number 2" },
            new Perio { Name = "2021 Number 3" }
        };

        return View(exams);
    }
}
